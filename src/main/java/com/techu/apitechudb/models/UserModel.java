package com.techu.apitechudb.models;

//IMPORTS
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

//INDICAMOS QUE ES UN DOCUMENTO (COLECCION(BBDD) = USERS)
@Document (collection = "users")
public class UserModel {

    @Id
    private String id;
    private String name;
    private int age;

    //CONSTRUCTORES DE LA CLASE
    public UserModel(){
    }

    //2º CONSTRUCTOR

    public UserModel (String id, String name, int age){
        this.id = id;
        this.name = name;
        this.age = age;
    }

    //GETTERS Y SETTERS PARA CADA UNO DE LOS ATRIBUTOS DE LA CLASE
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
