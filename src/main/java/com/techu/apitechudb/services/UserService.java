package com.techu.apitechudb.services;


//IMPORTS
import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class UserService {

    @Autowired
    UserRepository  userRepository;

    public List<UserModel> getUsers(String orderBy){
        System.out.println("getUsers en UserService");
        List<UserModel> result;

        if (orderBy != null){
            System.out.println("Se ha pedido ordenación");

            result = this.userRepository.findAll(Sort.by("age"));
        }else{
            System.out.println("No se ha pedido ordenación");

            result = this.userRepository.findAll();
        }

        return result;
    }

    //BUSCAR USUARIO POR ID
    public Optional<UserModel> findById(String id){
        System.out.println("findById en UserService");
        return this.userRepository.findById(id);
    }

    // AÑADIR NUEVO USUARIO
    public UserModel add(UserModel user){
        System.out.println("add en UserService");

        return this.userRepository.save(user);
    }

    //ACTUALIZAR USUARIO
    public UserModel update (UserModel userModel){
        System.out.println("Update en UserService");

        return this.userRepository.save(userModel);
    }

    //BORRAR USUARIO
    public boolean delete (String id){
        System.out.println("Delete en UserService");
        boolean result = false;

        if (this.findById(id).isPresent() == true){
            System.out.println("Usuario encontrado, borrando...");
            this.userRepository.deleteById(id);
            result = true;
        }

        return result;
    }
}
