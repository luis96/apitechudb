package com.techu.apitechudb.controllers;

//IMPORTS
import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

//@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE})
@RestController
@RequestMapping("/apitechudb/v3")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(
            @RequestParam (name = "$orderby", required = false) String orderBy){

        System.out.println("getUsers");

        return new ResponseEntity<>(
                this.userService.getUsers(orderBy),
                HttpStatus.OK
        );
    }

    //BUSCAR USER POR ID
    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id) {
        System.out.println("getUserById");
        System.out.println("La ID del cliente a buscar es: " + id);

        Optional<UserModel> user = this.userService.findById(id);

        return new ResponseEntity<>(
                user.isPresent() ? user.get() : "Usuario no encontrado",
                user.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    // CREAR NUEVO USER
    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("addUser");
        System.out.println("La ID del Usuario a crear es " + user.getId());
        System.out.println("El nombre del Usuario a crear es " + user.getName());
        System.out.println("La edad del Usuario a crear es " + user.getAge());

        return new ResponseEntity<>(
                this.userService.add(user),
                HttpStatus.CREATED
        );
    }

    //ACTUALIZAR DATOS USER POR ID
    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(
            @RequestBody UserModel userModel, @PathVariable String id){
        System.out.println("updateUser");
        System.out.println("La ID del Usuario a actualizar en parametro URL es " +  id);
        System.out.println("La ID del Usuario a actualizar es " + userModel.getId());
        System.out.println("El nombre del Usuario a actualizar es " + userModel.getName());
        System.out.println("La edad del Usuario a actualizar es " + userModel.getAge());

        Optional<UserModel> userToUpdate = this.userService.findById(id);

        if (userToUpdate.isPresent()){
            System.out.println("Usuario para actualizar encontrado, actualizando...");

            this.userService.update(userModel);
        }

        return  new ResponseEntity<>(
                userModel,
                userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    //BORRAR USUARIO POR ID
    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){
        System.out.println("deleteUser");
        System.out.println("El ID del Usuario a borrar es " + id);

        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "Usuario Borrado": "Usuario no Borrado",
                deleteUser ? HttpStatus.OK: HttpStatus.NOT_FOUND
        );
    }
}
