package com.techu.apitechudb.repositories;

//IMPORTS
import com.techu.apitechudb.models.ProductModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends MongoRepository<ProductModel, String> {

}
